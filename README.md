# Layerbox

Layerbox is a reimagining of toolbox to use chroot environments layered over the OS as
an alternative to toolbox which uses containers via podman.

# Groan

Layerbox is constructed with `groan` a framework for assembling scripts and tools in any languages
into a single hierarchical tool that may be deployed as a single deliverable.
