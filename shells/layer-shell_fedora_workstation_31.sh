#!/bin/bash
DEBUG=${DEBUG:-false}
LOUD=${LOUD:-true}

LAYER="${LAYER:-LAYER}"

# pre-requisites

[[ ! `sudo secon -r` == 'unconfined_r' ]] && echo "needs root/sysadm.role" 	&& exit 1
[[ ! -e /usr/bin/systemd-run ]] 		  && echo "need /usr/bin/systemd-run" && exit 1
 
# Set a trap to clean up after ourselves
mounts=()
trap_handler () {
  $DEBUG && echo sudo umount "${mounts[*]}"
  sudo umount -t overlay ${mounts[*]} || :
  sudo rm -rf "$CHROOT";
}
${AUTOCLEAN:-true} && trap trap_handler EXIT

# PREP CHROOT
CHROOT=$(mktemp -t "CH-$USER-$LAYER.XXXXXX" -d)

# Root level mount points
sudo mkdir -p "$CHROOT/host" "$CHROOT/boot" "$CHROOT/run" "$CHROOT/root" "$CHROOT/home"  "$CHROOT/var" 
# MountAPIVFS provides /dev /sys /proc
# PrivateTmp provides /tmp

# Recreate Symbolic Links as we find them on the host
links=($(find / -maxdepth 1 -type l -exec readlink {} \;))
for link in "${links[@]}"; do
  $DEBUG && printf "Recreating Link $link: "
  case $link in
     usr/*|run/*|sysroot/*|var/*)
     	  $DEBUG && echo ln -s $link $CHROOT/${link#*/}
     	  sudo ln -s $link $CHROOT/${link#*/}
     ;;
     var/roothome*)
#     	 $DEBUG && echo ln -s host/$link $CHROOT/root
#     	 sudo ln -s host/$link $CHROOT/root
     ;;
#     var/*)
#     	 $DEBUG && echo ln -s host/$link $CHROOT/${link#*/}
#     	 sudo ln -s $link $CHROOT/${link#*/}
#     ;;
  esac
done

# At this point we have the remaining to consider:
# Fedora31: /lost+found /media /mnt /opt /srv /var/*
# 	account  adm  cache  crash  db  empty  ftp  games  
# 	kerberos  lib  local  lock  log  mail  nis  opt  preserve  run  spool  tmp  www  yp
# Later we risk sharing /var/cache with everyone else.

$DEBUG && ls -l $CHROOT

# PREP the LAYER
layer=("/usr" "/etc" "/var")

# CoreOS has /usr/local -> /var/usrlocal
if [[ -e "/var/usrlocal" ]]; then #core os
	layer+=("/usr/local" )
else # we add a general var overlay just to catch anything that falls outside of /var/lib /var/cache /var/log
	: #layer+=("/var")
fi

for this in "${layer[@]}"; do
  case $this in
     /usr/local)
        lower="/var/usrlocal" #> different for coreos
     ;;
     *)
        lower="$this"        
     ;;
  esac
  
  if [[ -e "$lower" ]]; then
	mounts+=("$CHROOT$this")
	upper="${HOME}/.layers/${LAYER}"
	
	# We cant make an upperdir that somehow includes ourself as a lowerdir!
	# On FCOS /home is stored under /var/home and breaks this rule
	# This workaround puts offending upperdirs under /etc
	if [[ "${this%%/var*}" == "" ]]; then
		altupper="/etc/layers/${USER}/${LAYER}"
	    sudo ln -s "$altupper/var" "${upper}" || :
	    upper="$altupper"
	fi
	sudo mkdir -p "$upper$this" "$upper/.work$this" "$CHROOT$this"
   	sudo mount -t overlay overlay -o "lowerdir=$lower,upperdir=$upper$this,workdir=$upper/.work$this" "$CHROOT$this"
   	$DEBUG && echo mounted "$lower+$upper$this($upper/.work$this) -> $CHROOT$this"
  fi
done

for vardir in /var/*;
do
	echo "vardir: $vardir"
done

#AUTOQUIT is provided for testing - set to '--version' if you dont want to hang around in the shell
${AUTOQUIT:-false} && AUTOQUIT="--version" || AUTOQUIT=""

sudo --preserve-env systemd-run \
    -p Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    -p Environment=HOME=/root \
    -p RootDirectory="$CHROOT" \
    -p MountAPIVFS=yes \
    -p BindPaths="$HOME:/root /run /var/log /var/cache" \
    -p BindReadOnlyPaths='/:/host:norbind /boot /home' \
    -p PrivateTmp=yes \
    -t /bin/bash $AUTOQUIT --rcfile /etc/bash.in.pkg.rc

$LOUD && echo "Layer Shell Exited!"
