# returns $shell_script - path to shell script crafted for this platform
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

identifyPlatform # groan utility function

shell_script="$DIR/layer-shell_${ID}_${VARIANT_ID}_${VERSION_ID}.sh"

if [[ ! -e "$shell_script" ]]; then
	shell_script="$DIR/shells/layer-shell_${ID}_${VARIANT_ID}.sh"
fi

if [[ ! -e "$shell_script" ]]; then
	$LOUD && echo "Shell script ${shell_script} not found"
	return
fi