#!/bin/bash
$DEBUG && echo "${dim}${BASH_SOURCE}${reset}"

command="test-cleanup"
description="test of cleanup for one users layer"
usage="$breadcrumbs test-cleanup"

$SHOWHELP && executeHelp
$METADATAONLY && return

# Run this script to test the cleanup and clobber commands

LAYER="${LAYER:-LAYER}"

#[[ "0" != $(echo "/tmp/CH-$USER-$LAYER"* | wc -w) ]] && echo "Failed due to pre-existing CHROOT" && exit 1

AUTOCLEAN=false AUTOQUIT=true fcos layer enter

$LOUD && echo "Testing expected state..."

sudo sh <<SCRIPT
 	
	result=$(find "/tmp" -mindepth 1 -maxdepth 1 -name "CH-$USER-${LAYER}*" | wc -l)

	[[ "1" != "\$result"  ]] && echo "Expected CHROOT to be present (result \$result)" 
 
	result=$(mount -t overlay | grep -c "CH-$USER-$LAYER")

	[[ "3" != "\$result" ]] && echo "Expected 3 matching mounts found \$result" 
 
	result=$(find  "/etc/layers/keith/$LAYER" -maxdepth 1 -mindepth 1 | wc -l)

	[[ "2" != "\$result" ]] && echo "Expected 2 subdirs found \$result" 

	result=$(find "$HOME/.layers/$LAYER" -maxdepth 1 -mindepth 1 | wc -l)

	[[ "4" != "\$result" ]] && echo "Expected 4 subdirs found \$result"
	
	exit 0

SCRIPT

$LOUD && echo "Cleaning Up"

fcos layer cleanup

$LOUD && echo "Verifying clean up..."

sudo sh <<SCRIPT2
 	
	result=$(find "/tmp" -mindepth 1 -maxdepth 1 -name "CH-$USER-${LAYER}*" | wc -l)
 
	[[ "0" != "\$result"  ]] && echo "Expected CHROOT to be gone (result \$result)" && exit 1
 
	result=$(mount -t overlay)
 
	[[ "" != "\$result" ]] && echo "Expected no matching mounts" && exit 1
  
	result=$(ls "/etc/layers/keith/$LAYER" || echo "fail")
 
	[[ "fail" != "\$result" ]] && echo "Expected /etc/layers/keith/$LAYER to be gone" && exit 1

	result=$(ls "$HOME/.layers/$LAYER" || echo "fail")
 
	[[ "fail" != "\$result" ]] && echo "Expected "$HOME/.layers/$LAYER" to be gone" && exit 1
 
    exit 0
    
SCRIPT2

echo "PASS"