$DEBUG && echo "${dim}${BASH_SOURCE}${reset}"

command="install-shell"
description="install /usr/local/bin/layer-shell for this platform"
usage="usage:
$breadcrumbs install-shell --confirm"

$SHOWHELP && executeHelp
$METADATAONLY && return

$DEBUG && echo "Command: '$command'"

needs_sudo

$LOUD && echo "installing /usr/local/bin/layer-shell for this platform"

identifyPlatform # utility function

source "$scriptDir/../shells/which.sh" # returns $shell_script

$DEBUG && echo "layer-shell: $shell_script"

shell_script=$(realpath "$shell_script")

#we handle comments in multiline bash scripts
$CONFIRM && sudo sh -c "grep -v ^# '$shell_script' > /usr/local/bin/layer-shell"
$CONFIRM && sudo chmod a+x /usr/local/bin/layer-shell

$CONFIRM || echo "--dry-run (supply --confirm to make it happen)"