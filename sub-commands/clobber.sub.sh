$DEBUG && echo "${dim}${BASH_SOURCE}${reset}"

command="clobber"
description="Delete the LAYER completely"
usage="usage:
$breadcrumbs clobber"

$SHOWHELP && executeHelp
$METADATAONLY && return

for MOUNTPOINT in $(mount -t overlay | cut -d ' ' -f3)
do
	sudo umount -t overlay $MOUNTPOINT
done 

#delete all layers
sudo \rm -rf "/etc/layers"
sudo \rm -rf /home/*/.layers

#delete all chroots
sudo \rm -rf /tmp/CH-*

echo "done"