$DEBUG && echo "${dim}${BASH_SOURCE}${reset}"

command="cleanup"
description="Clean up wayward chroots and overlay mounts"
usage="usage:
$breadcrumbs cleanup"

$SHOWHELP && executeHelp
$METADATAONLY && return

LAYER="${LAYER:-LAYER}"

for MOUNTPOINT in $(mount -t overlay | cut -d ' ' -f3 | grep "CH-$USER-$LAYER")
do
	sudo umount -t overlay $MOUNTPOINT
done 

#delete all layers
del="${HOME}/.layers/${LAYER}"
sudo \rm -rf "$del"
del="/etc/layers/${USER}/${LAYER}"
sudo \rm -rf "$del"

#delete all chroots
del="/tmp/CH-${USER}-${LAYER}"
sudo \rm -rf "$del"*

$LOUD && echo "cleanup complete"

exit 0