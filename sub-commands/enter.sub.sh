$DEBUG && echo "${dim}${BASH_SOURCE}${reset}"

command="shell"
description="start a psuedo-root shell in a LAYER"
usage="usage:
$breadcrumbs shell"

$SHOWHELP && executeHelp
$METADATAONLY && return

LAYER="${LAYER:-LAYER}"

$LOUD && echo "Entering layer: ${LAYER}"

identifyPlatform # utility function

source "$scriptDir/../shells/which.sh"

#we handle comments in multiline bash scripts
source <(grep -v ^# "$shell_script")
  
exit 0